
# lgp-front

The frontend is based on react. And I also use a library called [react bootsrap](https://react-bootstrap.github.io). 

## Prerequisites 
You need to have [node.js](https://nodejs.org/en/) installed to run it. 

## How to run 
1. cd into the lgp-front folder, ```npm install``` to install the dependency
2. You might also need to run ```npm update```, ```npm audit fix --force``` to update the dependency
3. To start the server, ```npm start``` will start a server at http://localhost:3000 in your browser. 

## Tools
I use vsc as the ide. And I use React Developer Tools extension in chrome to view the components. 

## Tasks

- [ ] Home page 
    - [ ] login and sign up button
    - [ ] top nav bar 
    - [ ] background picture
- [ ] Login/Signup modal(page)
    - [ ] login form 
    - [ ] cookie 
- [ ] Profile page 
    - [ ] Add, delete, modify personal information 
- [ ] Dashboard page
    - [ ] display other users profile picture + information
    - [ ] like button 
    - [ ] unlike button 
- [ ] Chat page (If we have time)


