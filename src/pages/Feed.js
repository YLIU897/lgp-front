import { CheckCircleIcon, XCircleIcon } from "@heroicons/react/outline";
import React from "react";
import { useAuthenticator } from "@aws-amplify/ui-react";

export default function Feed() {
  const { user, signOut } = useAuthenticator((context) => [context.user]);
  //TODO fetch user profiles using id token
  console.log(user.getSignInUserSession().getIdToken().getJwtToken());
  return (
    <>
      <div className="flex container justify-between items-center mx-auto">
        <h2 className="text-xl font-semibold text-gray-800 dark:text-white">
          Feed
        </h2>
      </div>
      <div className="pt-10">
        <div className="w-3/4 mx-auto bg-white rounded-lg shadow-lg dark:bg-gray-700 text-gray-800 dark:text-slate-100 p-5">
          <div className="flex">
            <div className="w-1/3 ">
              <img
                className="object-cover w-full h-56"
                src="https://news.wisc.edu/content/uploads/2018/05/Bucky_Parade18_6759-1-1024x681.jpg"
                alt="avatar"
              ></img>

              <div className="py-5 text-center">
                <a href="#" className="block text-2xl font-bold" role="link">
                  John
                </a>
                <span className="text-sm text-gray-700 dark:text-gray-200">
                  Bio
                </span>
              </div>
            </div>
            <div className="w-2/3">
              <p className="p-5">
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Officiis exercitationem delectus, expedita voluptas dolore animi
                tempore ipsum eos magnam explicabo, doloribus impedit aliquid
                id, pariatur minima. Quas doloremque fugiat fugit.
              </p>
              <div className="flex w-80 mx-auto mt-20 items-center content-center">
                <XCircleIcon className="flex-1 w-10 h-10" stroke="#e3242b" />
                <CheckCircleIcon
                  className="flex-1 w-10 h-10"
                  stroke="#03ac13"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
