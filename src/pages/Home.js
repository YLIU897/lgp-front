import { Container, Row, Col } from "react-bootstrap";
import { useState } from "react";

import AuthenticateModal from "../components/AuthenticateModal";
import Navgation from "../components/Nav";

const Home = () => {
  // const authToken = true;
  const [showModal, setShowModal] = useState(false);
  const [isLogin, setIsLogin] = useState(true);
  const handleShowModal = () => {
    setShowModal(true);
  };

  const handleSignup = () => {
    handleShowModal();
    setIsLogin(false);
  };

  const handleLogin = () => {
    handleShowModal();
    setIsLogin(true);
  };

  return (
    <div className="overlay">
      <Navgation handleLogin={handleLogin} />
      <Container fluid className="home_container">
        <Row className="justify-content-md-center">
          <Col>
            <h1>LGP</h1>
          </Col>
        </Row>
        <Row className="justify-content-md-center pb-5 mb-5">
          <Col>
            <button className="sign_up_btn" onClick={handleSignup}>
              Sign Up
            </button>
          </Col>
        </Row>
      </Container>
      {showModal && (
        <AuthenticateModal
          setShowModal={setShowModal}
          isLogin={isLogin}
          setIsLogin={setIsLogin}
        />
      )}
    </div>
  );
};

export default Home;
