import React from "react";
import DarkModeSettings from "../components/DarkModeSettings";
import { useAuthenticator } from "@aws-amplify/ui-react";

export default function Settings() {
  const { user, signOut } = useAuthenticator((context) => [context.user]);
  return (
    <div className="bg-white text-black dark:bg-black dark:text-white transition-colors">
      <div className="container mx-auto pt-10">
        <DarkModeSettings />
        <button
          onClick={signOut}
          className="block w-full text-red-600 text-center p-3"
        >
          Sign Out
        </button>
      </div>
    </div>
  );
}
