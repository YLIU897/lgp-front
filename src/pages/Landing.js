import { Link } from "react-router-dom";

export default function Landing() {
  //TODO add style to homepage
  return (
    <div className="bg-black text-white h-screen">
      <div className="container mx-auto pt-16">
        <Link to="/login">
          <div className="bg-blue-700 p-3 text-center">Login or Sign Up</div>
        </Link>

        <p>This will be the home/landing page</p>
      </div>
    </div>
  );
}
