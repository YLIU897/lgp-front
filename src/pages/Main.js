import React from "react";
import { useAuthenticator } from "@aws-amplify/ui-react";
import Feed from "./Feed";
import Landing from "./Landing";

export default function Main() {
  const { route } = useAuthenticator((context) => [context.route]);
  return route === "authenticated" ? <Feed /> : <Landing />;
}
