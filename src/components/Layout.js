import React from "react";
import { Outlet } from "react-router-dom";
import { useAuthenticator } from "@aws-amplify/ui-react";
import {
  BellIcon,
  CogIcon,
  HomeIcon,
  InboxIcon,
} from "@heroicons/react/outline";
import { Link } from "react-router-dom";

export function Layout() {
  const { user, route, signOut } = useAuthenticator((context) => [
    context.route,
    context.signOut,
  ]);

  return route === "authenticated" ? (
    <div className="flex">
      <div className="flex flex-col w-64 h-screen px-4 py-8 bg-white dark:bg-gray-900 border-r dark:border-gray-700">
        <h2 className="text-3xl ml-2 font-semibold text-gray-800 dark:text-white">
          Gym
        </h2>

        <div className="flex flex-1 flex-col justify-between mt-5">
          <nav>
            <Link
              className="flex items-center p-2 mt-4 text-gray-600 dark:text-gray-400 hover:bg-gray-200 dark:hover:bg-gray-700 hover:text-gray-700 dark:hover:text-gray-200 transition-colors duration-300 rounded-md"
              to="/"
            >
              <HomeIcon className="h-5 w-5" />

              <span className="mx-3 font-medium">Home</span>
            </Link>
            <a
              className="flex items-center p-2 mt-4 text-gray-600 dark:text-gray-400 hover:bg-gray-200 dark:hover:bg-gray-700 hover:text-gray-700 dark:hover:text-gray-200 transition-colors duration-300 rounded-md"
              href="#"
            >
              <InboxIcon className="h-5 w-5" />

              <span className="mx-3 font-medium">Messages</span>
            </a>
            <a
              className="flex items-center p-2 mt-4 text-gray-600 transition-colors duration-300 transform rounded-md dark:text-gray-400 hover:bg-gray-200 dark:hover:bg-gray-700 dark:hover:text-gray-200 hover:text-gray-700"
              href="#"
            >
              <BellIcon className="h-5 w-5" />

              <span className="mx-3 font-medium">Notification</span>
            </a>

            <Link
              className="flex items-center p-2 mt-4 text-gray-600 dark:text-gray-400 hover:bg-gray-200 dark:hover:bg-gray-700 hover:text-gray-700 dark:hover:text-gray-200 transition-colors duration-300 rounded-md"
              to="/settings"
            >
              <CogIcon className="h-5 w-5" />

              <span className="mx-3 font-medium">Settings</span>
            </Link>
          </nav>

          <div className="flex items-center px-2">
            <img
              className="object-cover object-center mx-2 h-9 w-9 rounded-full"
              src="https://pbs.twimg.com/profile_images/773019754055557120/BQlB5Tlj_400x400.jpg"
              alt="avatar"
            />
            <h4 className="mx-2 font-medium text-gray-800 dark:text-gray-200 cursor-pointer">
              {user.username}
            </h4>
          </div>
        </div>
      </div>
      <div className="h-screen flex-1 p-12 bg-white border-r dark:bg-gray-900 dark:border-gray-700">
        <Outlet />
      </div>
    </div>
  ) : (
    <Outlet />
  );
}
