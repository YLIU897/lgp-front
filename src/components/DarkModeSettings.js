import { RadioGroup } from "@headlessui/react";
import React, { useState } from "react";

//TODO css styling
export default function DarkModeSettings() {
  const [theme, setTheme] = useState(getInitialTheme());

  return (
    <div>
      <RadioGroup
        value={theme}
        onChange={(val) => {
          setTheme(val);
          changeTheme(val);
        }}
      >
        <RadioGroup.Label>Appearance</RadioGroup.Label>
        <RadioGroup.Option
          value="Dark"
          className="bg-sky-900 p-5 cursor-pointer rounded-lg shadow-md focus:outline-none"
        >
          {({ checked }) => (
            <span className={checked ? "bg-red-500" : ""}>Dark</span>
          )}
        </RadioGroup.Option>
        <RadioGroup.Option
          value="Light"
          className="bg-sky-900 p-5 cursor-pointer rounded-lg shadow-md focus:outline-none"
        >
          {({ checked }) => (
            <span className={checked ? "bg-red-500" : ""}>Light</span>
          )}
        </RadioGroup.Option>
        <RadioGroup.Option
          value="System"
          className="bg-sky-900 p-5 cursor-pointer rounded-lg shadow-md focus:outline-none"
        >
          {({ checked }) => (
            <span className={checked ? "bg-red-500" : ""}>Auto</span>
          )}
        </RadioGroup.Option>
      </RadioGroup>
    </div>
  );
}

function getInitialTheme() {
  var theme;
  switch (localStorage.theme) {
    case "dark":
      theme = "Dark";
      break;
    case "light":
      theme = "Light";
      break;
    default:
      theme = "Auto";
      break;
  }

  changeTheme(theme);

  return theme;
}

function changeTheme(value) {
  switch (value) {
    case "System":
      localStorage.removeItem("theme");
      break;

    case "Dark":
      localStorage.theme = "dark";
      break;

    case "Light":
      localStorage.theme = "light";
      break;

    default:
      break;
  }
  if (
    localStorage.theme === "dark" ||
    (!("theme" in localStorage) &&
      window.matchMedia("(prefers-color-scheme: dark)").matches)
  ) {
    document.documentElement.classList.add("dark");
  } else {
    document.documentElement.classList.remove("dark");
  }
}
