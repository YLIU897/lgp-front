import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import { useState } from "react";

const AuthenticateModal = ({ setShowModal, isLogin, setIsLogin }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const saveEmail = (e) => {
    setEmail(e.target.value);
  };

  const savePassword = (e) => {
    setPassword(e.target.value);
  };

  const saveConfirmPassword = (e) => {
    setConfirmPassword(e.target.value);
  };

  const handleClose = () => {
    setShowModal(false);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(email);
    console.log(password);
    console.log(confirmPassword);
  };

  const switchForm = () => {
    setIsLogin(!isLogin);
  };

  return (
    <Container>
      <Modal
        size="md"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        backdrop="static"
        show={handleClose}
        onHide={handleClose}
      >
        <Modal.Header closeButton>
          <Modal.Title>{isLogin ? "Login" : "Sign Up"}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleSubmit}>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                required
                type="email"
                placeholder="Enter email"
                onChange={saveEmail}
                value={email}
              />
              <Form.Text>
                {isLogin ? (
                  <Button className="p-0" variant="link" onClick={switchForm}>
                    Don't have an account? Sign up here.
                  </Button>
                ) : (
                  <Button className="p-0" variant="link" onClick={switchForm}>
                    Already have an account? Login here.
                  </Button>
                )}
              </Form.Text>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Enter password"
                onChange={savePassword}
                value={password}
              />
            </Form.Group>
            {/* <Form.Group className="mb-3" controlId="formBasicCheckbox">
              <Form.Check type="checkbox" label="Remember me" />
            </Form.Group> */}
            {!isLogin && (
              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Confirm password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Confirm password"
                  onChange={saveConfirmPassword}
                  value={confirmPassword}
                />
              </Form.Group>
            )}
            <Button variant="primary" type="submit">
              {isLogin ? "Login" : "Sign Up"}
            </Button>
            {/* <p className="forgot-password text-right">
              Forgot <a href="#">password?</a>
            </p> */}
          </Form>
        </Modal.Body>
      </Modal>
    </Container>
  );
};

export default AuthenticateModal;
