import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import Main from "./pages/Main";
import Settings from "./pages/Settings";
import Onboard from "./pages/Onboard";
import RequireAuth from "./RequireAuth";
import ErrorPage from "./pages/ErrorPage";
import Login from "./pages/Login";
import { Layout } from "./components/Layout";

import { Amplify } from "aws-amplify";
import { Authenticator } from "@aws-amplify/ui-react";
import "@aws-amplify/ui-react/styles.css";
import awsExports from "./aws-exports.js";

Amplify.configure(awsExports);

function AllRoutes() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<Main />} />
          <Route
            path="/settings"
            element={
              <RequireAuth>
                <Settings />
              </RequireAuth>
            }
          />
          <Route
            path="/onboard"
            element={
              <RequireAuth>
                <Onboard />
              </RequireAuth>
            }
          />
        </Route>
        <Route path="/login" element={<Login />} />
        <Route path="*" element={<ErrorPage />} />
      </Routes>
    </BrowserRouter>
  );
}

function App() {
  return (
    <Authenticator.Provider>
      <AllRoutes />
    </Authenticator.Provider>
    //   <BrowserRouter>
    //   <Routes>
    //     <Route path="/" element={<Feed />} />
    //     <Route path="/dashboard" element={<Dashboard />} />
    //     <Route path="/onboard" element={<Onboard />} />
    //   </Routes>
    // </BrowserRouter>
  );
}

export default App;
